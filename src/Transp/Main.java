package Transp;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Main {
    public static void main(String[] args) {
        System.out.println(decrypt(4,"Asrssnfat tlasocu toec sd tg ret ocs otserrepw ar carhawc ameym v aoi iohcsal et prs si"));
    }

    public static String encrypt(int key, String msn){
        return IntStream.range(0, msn.length()).mapToObj(i -> String.valueOf(msn.charAt((i * key) % msn.length()))).collect(Collectors.joining());
    }
    public static String decrypt(int key, String msn){
        return encrypt(1+(msn.length()/key),msn);
    }
}
