package AES;

import java.util.Base64;

public class Main {
    public static void main(String[] args) throws Exception {
        System.out.println(AES.decrypt(
                "RF441svPTZz/YDKHGkVoRBDevEeERIoih6OHosUfcUxrMXCL0JVswGNKTGaOFMSyNeGv/OoEA5yGe6rES8qfwAbQPKrfh6ihM1gRENlzG85EvpMz1ZR9/tguZq195ptmWkA1RM8o",
                "Això és un password del 2022"
        ));
        System.out.println(AES.decrypt(
                convertHexTo64("f4fd6ad48f782ecef75f2b50c065278f5fa5abf2ab2f758f16025f141b51545f62ea8be90a093f179f92d69c804ed6fcf21f408dea4380fa3b42e7dd8b169dd81129de98501d361225c41328ffddc0e57756f491dba7223d5f0ad7a5019c8c521b8727ca3fd208f5e73d511752cb04"),
                "Això és un password del 2022"
        ));
    }
    public static String convertHexTo64(String hex) {
        char[] toBase64 = new char[]{'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

        StringBuilder binary = new StringBuilder();
        for (int i = 0; i < hex.length(); i++) {
            StringBuilder bin = new StringBuilder(Integer.toBinaryString(Integer.parseInt(hex.charAt(i) + "", 16)));
            while(bin.length() < 4) bin.insert(0, '0');
            binary.append(bin);
        }
        StringBuilder b64 = new StringBuilder();
        for (int i = 0; i < binary.length()/6; i++) {
            b64.append(toBase64[Integer.parseInt(binary.substring(6*i, 6*i+6), 2)]);
        }
        return b64.toString();
    }
}
