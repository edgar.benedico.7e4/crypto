package Polybius;

public class Square {
    private char[][] m;

    public Square(int dim, String alph) {
        m = new char[dim][dim];
        for (int i = 0; i < dim; i++) {
            for (int j = 0; j < dim; j++) {
                m[i][j] = dim*i+j < alph.length() ? alph.charAt(dim*i+j) : m[i][j];
            }
        }
    }

    public char coordinateToChar(int x){
        return m[(x-10)/10][(x-1)%10];
    }
}
