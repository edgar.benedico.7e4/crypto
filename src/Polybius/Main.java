package Polybius;

public class Main {
    public static void main(String[] args) {

        Polybius polybius = new Polybius(7, "ABCÇDEFGHIJKLMNÑOPQRSTUVWXYZ0123456789-+*/., ?¿!!");

        int[] msn = new int[]{12, 33, 31, 11, 73, 41, 11, 36, 15, 11, 73, 11, 73, 41, 33, 41, 22, 33, 27, 76};

        System.out.println(polybius.decryptCoordinates(msn));

    }
}
