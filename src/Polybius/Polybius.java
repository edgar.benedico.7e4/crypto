package Polybius;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Polybius {
    private Square square;

    public Polybius(int squareDimension, String squareAlphabet) {
        square = new Square(squareDimension, squareAlphabet);
    }

    public String decryptCoordinates(int[] coordList){
       return Arrays.stream(coordList).mapToObj(c -> String.valueOf(square.coordinateToChar(c))).collect(Collectors.joining());
    }
}
