package Cesar;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Cesar {

    private HashSet<String> words;
    private String dictionary;

    public Cesar(HashSet<String> words,String dictionary) {
        this.words = words;
        this.dictionary = dictionary;
    }
    private boolean existInDictionary(char c){
        return dictionary.indexOf(c) != -1;
    }
    private char cesarFunction(char c, int k){
        return dictionary.charAt(Math.floorMod(dictionary.indexOf(c)+k,dictionary.length()));
    }
    public String encrypt(String msn, int key){
        StringBuilder str = new StringBuilder(msn);
        for (int i = 0; i < str.length(); i++) if (existInDictionary(Character.toLowerCase(str.charAt(i)))) str.setCharAt(i, cesarFunction(Character.toLowerCase(str.charAt(i)), key));
        return str.toString();
    }
    public String decrypt(String msn, int key){
        return encrypt(msn, -key);
    }
    public ArrayList<CesarForceSolution> forceDecrypt(String msn){
        return IntStream.range(0, dictionary.length()).mapToObj(i -> new CesarForceSolution(decrypt(msn, i), i, words)).sorted().collect(Collectors.toCollection(ArrayList::new));
    }
}
