package Cesar;

import java.util.Arrays;
import java.util.HashSet;

public class CesarForceSolution implements Comparable<CesarForceSolution>{
    private final String solution;
    private final int key;
    private final double localProbability;
    private final HashSet<String> words;

    public CesarForceSolution(String solution, int key, HashSet<String> words) {
        this.solution = solution;
        this.key = key;
        this.words = words;
        this.localProbability = CalculateLocalProbability();
    }

    private double CalculateLocalProbability() {
        return 100*Arrays.stream(solution.split("[,. !¡?¿]")).mapToDouble(t->words.contains(t)|| t.length() == 0 ? 1 : 0).sum() / solution.split("[,. !¡?¿]").length;
    }

    @Override
    public String toString() {
        return String.format("With a probability of %.2f%% with the password %d it generates the following message: %s", localProbability, key, solution);
    }

    @Override
    public int compareTo(CesarForceSolution c) {
        return (int) (c.localProbability-localProbability);
    }
}
