package Cesar;

import java.io.File;
import java.util.HashSet;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Cesar cesar = new Cesar(getWords("/home/sjo/Escriptori/DADES/Edgar/M09/crypto/src/Cesar/words.dat"),"abcdefghijklmnopqrstuvwxyz");
        //String code = cesar.encrypt(msn, 0);
        //String decode = cesar.decrypt(code, key);

        String msn1 = "Gjsjinhy Hzrgjwgfyhm jx fzs fhytw gwnyfsnh ij ya, yjfywj, hnsj n itgqfylj";

        cesar.forceDecrypt(msn1).forEach(System.out::println);
        System.out.println("\nSolution is:\n"+cesar.forceDecrypt(msn1).get(0));

        String msn2 = "cebtenzzvat, zdgureshpxre";
        cesar.forceDecrypt(msn2).forEach(System.out::println);
        System.out.println("\nSolution is:\n"+cesar.forceDecrypt(msn2).get(0));
    }

    private static HashSet<String> getWords(String path){
        HashSet<String> words = new HashSet<>();
        try {
            Scanner f = new Scanner(new File(path));
            while (f.hasNext())  words.add(f.nextLine());
        } catch (Exception ignored) { }
        return words;
    }
}
